package convexHull;
import unit4.collectionsLib.Queue;
import unit4.collectionsLib.Stack;
import unit4.turtleLib.*;
import java.awt.*;
import java.util.Scanner;
import java.util.*;
public class convexHull
{
	public static void main(String[] args)
    {
        final int WIDTH = 1520;
        final int HEIGHT = 826;
		Scanner in = new Scanner(System.in);
        System.out.println("please enter how many points do you want");
        int numOfPoints = in.nextInt();
        Point  points [] = generatePoints(0, WIDTH, 0, HEIGHT, numOfPoints);
        //put the smallest point in the first place in the array
        moveLowestPointToStart(points);
        //sort the array by each point's slope to p[0]
        bubbleSort(points);
        insertP0(points);


        Point[] validPoints = getConvexHull(points);
        new convexGraphics(points, validPoints);
    }
	public static void moveTo(Point p, Turtle t)
    {
		if(p.getX()==t.getXLocation())//the turtle is above or below the target point
		{
			if(p.getY()>t.getYLocation())
			{				
				t.turnLeft(90);
			}
			else
			{
				t.turnRight(90);
			}
			t.moveForward(Math.abs(p.getY()-t.getYLocation()));
		}
		else
        {
			double m = (t.getYLocation()-p.getY())/(t.getXLocation()-p.getX());
			double alpha = Math.toDegrees(Math.atan(m));
			double dist = Math.sqrt(Math.pow(t.getXLocation()-p.getX(), 2)+Math.pow(t.getYLocation()-p.getY(), 2));
			if(p.getX()<t.getXLocation())
			{
				alpha+=180;
			}
			t.turnLeft(alpha);
			t.moveForward(dist);
		}
		//reset heading
		t.turnRight(t.getHeading()+90);
	}

	static Point[] generatePoints(int lowerBoundWidth, int upperBoundWidth,int lowerBoundHeight, int upperBoundHeight, int numOfPoints)
    {
        int width = upperBoundWidth-lowerBoundWidth;
        int height = upperBoundHeight-lowerBoundHeight;
        Random r = new Random();
        Point[] points = new Point[numOfPoints];
        //generate random points
        for(int i = 0; i < numOfPoints; i++)
        {
            points[i] = new Point(r.nextInt(width)+lowerBoundWidth, r.nextInt(height)+lowerBoundHeight);
        }
        return points;
    }
    static void moveLowestPointToStart(Point[] points)
    {
        //find point with smallest y
        Point smallest = points[0];
        int smallestIndex = 0;
        for(int i = 0; i< points.length; i++)
        {
            if(points[i].getY()<smallest.getY() || points[i].getY()==smallest.getY() && points[i].getX()<smallest.getX())
            {
                smallest = points[i];
                smallestIndex = i;
            }
        }
        points[smallestIndex] = points[0];
        points[0] = smallest;
    }
	//return -1 if p1>p2 0 if p1==p2 1 if p2>p1
	public static int compare(Point origin, Point p1, Point p2)
	{
		double m2;
		double m1;

		if(p1.equals(p2))//points are equal
		{
			return 0;
		}
		if(p1.getX() == origin.getX())
		{
			return 1;
		}
		if(p2.getX() == origin.getX())
		{
			return -1;
		}
		//get the slope of each point to the original point
		m1 = (origin.getY()-p1.getY())/(origin.getX()-p1.getX());
		m2 = (origin.getY()-p2.getY())/(origin.getX()-p2.getX());
		if(m1>m2)
		{
			return -1;
		}
		if(m2>m1)
		{
			return 1;
		}
		return 0;
	}
	public static void bubbleSort(Point[] p)
	{
		int count = -1;
		while(count != 0)
		{
			count = 0;
			for(int i = 1; i < p.length - 1; i++)
			{
				int larger = compare(p[0], p[i], p[i + 1]);
				if(larger == -1) {
                    Point temp = p[i + 1];
                    p[i + 1] = p[i];
                    p[i] = temp;
                    count++;
                }
            }
            System.out.println(count);
        }
	}
	static void insertP0(Point[] points)
    {
        Queue<Point> q  = new Queue<>();
        boolean drawn = false;
        //create queue with p[0] in correct place
        if(points[0].getX()!=points[1].getX() && (points[0].getY()-points[1].getY())/(points[0].getX()-points[1].getX())>=0)
        {
            q.insert(points[0]);
            drawn = true;
        }
        for(int i =1; i < points.length; i++)
        {
            q.insert(points[i]);
            if(!drawn&&i==points.length-1 || !drawn && points[0].getX()!=points[i+1].getX() && (points[0].getY()-points[i+1].getY())/(points[0].getX()-points[i+1].getX())>=0)
            {
                q.insert(points[0]);
                drawn = true;
            }
        }
        while(!q.head().equals(points[0]))
        {
            q.insert(q.remove());
        }
        q.insert(q.remove());
        int j =0;
        //empty queue into array and draw all points
        while(!q.isEmpty())
        {
            points[points.length-j-1] = q.remove();
            j++;
        }
    }

    static Point[] getConvexHull(Point[] points)
    {

        System.out.println();
        Stack<Point> validPoints = new Stack<>();
        validPoints.push(points[0]);
        validPoints.push(points[1]);
        for(int i=2; i<points.length-1; i++)
        {
            validPoints.push(points[i]);
            int distTo0 =  getDistSign(validPoints.top(), getNToTop(validPoints, 1), points[0]);
            int distToNext = getDistSign(validPoints.top(), getNToTop(validPoints, 1), points[i + 1]);
            while (getNToTop(validPoints, 2)!= null && (distToNext !=
                    distTo0 || distToNext== 0)) {
                validPoints.pop();
                distTo0 =  getDistSign(validPoints.top(), getNToTop(validPoints, 1), points[0]);
                distToNext = getDistSign(validPoints.top(), getNToTop(validPoints, 1), points[i + 1]);
            }
        }
        validPoints.push(points[points.length-1]);
        int stackSize = getStackSize(validPoints);
        Point[] out = new Point[stackSize];
        for(int i =0; i<stackSize; i++)
        {
            out[i] = validPoints.pop();

        }
        return out;
    }
	static int getDistSign(Point a1, Point a2, Point b)
    {
    	//check if the line is slope-less
        if(a1.getX()==a2.getX())
        {
            return b.getX()<a1.getX() ? -1 : 1;
        }
        //get the slope of the line
        double m = (a1.getY()-a2.getY())/(a1.getX()-a2.getX());
        //get the distance from the point b to the line a1a2
        double dist = b.getY()-m*b.getX()+m*a2.getX()-a2.getY();
        //get the sign of the distance
        int sign = (int)(dist/Math.abs(dist));
        return sign;
    }
    static Point getNToTop(Stack<Point> s, int n)
    {
        Stack<Point> temps = new Stack<>();
        Point p;
        //remove all points until the right point is reached
        for (int i = 0; i < n && !s.isEmpty(); i++) {
            temps.push(s.pop());
        }
        //if stack is empty return all points to stack and return null
        if(s.isEmpty())
        {
            while (!temps.isEmpty()) {
                s.push(temps.pop());
            }
            return null;
        }
        //save the right point and return all points to stack
        p = s.top();
        while (!temps.isEmpty()) {
            s.push(temps.pop());
        }
        return p;
    }
    static int getStackSize(Stack<Point> s)
    {
        int counter = 0;
        Stack<Point> temp = new Stack<Point>();
        while(!s.isEmpty())
        {
            temp.push(s.pop());
            counter++;
        }
        while(!temp.isEmpty())
        {
            s.push(temp.pop());
        }
        return counter;
    }
}
