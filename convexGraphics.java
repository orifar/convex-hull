package convexHull;

import java.awt.*;
import javax.swing.JFrame;
import javax.swing.JPanel;


@SuppressWarnings("serial")
class convexGraphics extends JFrame {
    convexGraphics(Point[] points, Point[] convexPoints ) {
        Polygon p= new Polygon();

        setTitle("my frame");

        DrawPane pane = new DrawPane(getXArr(points), getYArr(points), getXArr(convexPoints), getYArr(convexPoints));
        this.getContentPane().add(pane);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(true);
        setVisible(true);
        setSize(1560, 860);
        pane.start();
    }
    static int[] getYArr(Point[] points)
    {
        int[] y = new int[points.length];
        for(int i =0;i<points.length;i++)
        {
            y[i] = 826-points[i].y;
        }
        return y;
    }
    static int[] getXArr(Point[] points)
    {
        int[] x = new int[points.length];
        for(int i =0;i<points.length;i++)
        {
            x[i] = points[i].x;
        }
        return x;
    }

    class DrawPane extends JPanel {
        int[] _x;
        int[] _y;
        int[] _cX;
        int[] _cY;
        DrawPane(int[] x, int[] y, int[] cX, int[] cY) {
            this._x = x;
            this._y = y;
            this._cX = cX;
            this._cY = cY;
        }

        void start() {

        }
        public void paintComponent(Graphics g) {
            //Paint stuff
            super.paintComponent(g);
            Graphics2D g2 = (Graphics2D) g;
            g2.setStroke(new BasicStroke(4));
            g2.setColor(new Color(255,0,0));
            for(int i=0; i<_x.length; i++)
            {
                g2.drawOval(_x[i], _y[i], 4, 4);
            }
            g2.setColor(new Color(0,0,0));
            g2.drawPolygon(_cX, _cY, _cX.length);
        }
    }
}